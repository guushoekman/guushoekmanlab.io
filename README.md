# guushoekman.gitlab.io

Repo for [guushoekman.com](https://guushoekman.com) using GitLab pages.

## Submodules

Because of the submodules, clone the repo with `git clone --recursive git@gitlab.com:guushoekman/guushoekman.gitlab.io.git`

Also remember to update the submodules when making changes: `git submodule foreach git pull origin master`